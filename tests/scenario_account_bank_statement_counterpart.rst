===========================================
Account Bank Statement Counterpart Scenario
===========================================

Imports::

    >>> import datetime
    >>> import pytz
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> today = datetime.date.today()
    >>> now = datetime.datetime.now()

Install account_bank_statement_counterpart::

    >>> config = activate_modules('account_bank_statement_counterpart')

Create company::

    >>> _ = create_company()
    >>> company = get_company()
    >>> company.timezone = 'Europe/Madrid'
    >>> company.save()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']
    >>> cash.bank_reconcile = True
    >>> cash.reconcile = True
    >>> cash.save()

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()

Create Journal::

    >>> Sequence = Model.get('ir.sequence')
    >>> SequenceType = Model.get('ir.sequence.type')
    >>> sequence_type, = SequenceType.find([('name', '=', 'Account Journal')])
    >>> sequence = Sequence(name='Bank', sequence_type=sequence_type,
    ...     company=company)
    >>> sequence.save()
    >>> AccountJournal = Model.get('account.journal')
    >>> account_journal = AccountJournal(name='Statement',
    ...     type='cash',
    ...     sequence=sequence)
    >>> account_journal.save()

Create Statement Journal::

    >>> StatementJournal = Model.get('account.bank.statement.journal')
    >>> statement_journal = StatementJournal(name='Test',
    ...     journal=account_journal, currency=company.currency, account=cash)
    >>> statement_journal.save()

Create Bank Move::

    >>> period = fiscalyear.periods[0]
    >>> Move = Model.get('account.move')
    >>> move = Move()
    >>> move.period = period
    >>> move.journal = account_journal
    >>> move.date = period.start_date
    >>> line = move.lines.new()
    >>> line.account = receivable
    >>> line.debit = Decimal('80.0')
    >>> line.party = party
    >>> line2 = move.lines.new()
    >>> line2.account = revenue
    >>> line2.credit = Decimal('80.0')
    >>> move.click('post')
    >>> move.state
    'posted'
    >>> line1, line2 = move.lines

Create Bank Statement With Different Curreny::

    >>> BankStatement = Model.get('account.bank.statement')
    >>> statement = BankStatement(journal=statement_journal, date=now)

Create Bank Statement Lines::

    >>> MoveLine = Model.get('account.move.line')
    >>> StatementLine = Model.get('account.bank.statement.line')
    >>> Line = Model.get('account.move.line')
    >>> statement_line = StatementLine()
    >>> statement.lines.append(statement_line)
    >>> statement_line.date = now
    >>> statement_line.description = 'Statement Line'
    >>> statement_line.amount = Decimal('80.0')
    >>> statement.click('confirm')
    >>> statement.state == 'confirmed'
    True

    >>> statement_line, = statement.lines
    >>> statement_line.reload()
    >>> line2.reload()
    >>> statement_line.counterpart_lines.append(Line(line2.id))
    >>> statement_line.save()
    >>> statement_line.click('post')

Check reconciliation::

    >>> line2.reload()
    >>> move_line, = [x for x in line2.reconciliation.lines if x != line2]
    >>> move_line.account == line2.account
    True
    >>> move_line.credit ==  Decimal('80.0')
    True
    >>> move_line2, = [x for x in move_line.move.lines if x != move_line]
    >>> move_line2.account == statement_line.account
    True
    >>> move_line2.debit == Decimal('80.0')
    True
    >>> receivable.reload()
    >>> receivable.balance == Decimal('0.00')
    True

Unreconcile lines::

    >>> line2.reconciliation != None
    True
    >>> unreconcile = Wizard('account.move.unreconcile_lines', [move_line, move_line2])
    >>> len(unreconcile.form.reconciliations)
    1
    >>> unreconcile.form.reconciliations[0].reconciliation == line2.reconciliation
    True
    >>> len(unreconcile.form.reconciliations[0].bank_statement_lines)
    1
    >>> unreconcile.execute('continue_')
    >>> line2.reload()
    >>> line2.reconciliation != None
    False

Not allow cancel when period is closed::

    >>> statement_line.click('post')
    >>> Period = Model.get('account.period')
    >>> JournalPeriod = Model.get('account.journal.period')
    >>> periods = Period.find([])
    >>> Period.click(periods, 'close')
    >>> statement_line.click('cancel') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    PeriodNotFoundError: ...

    >>> Period.click(periods, 'reopen')
    >>> journal_periods = JournalPeriod.find([
    ...     ('period', 'in', [p.id for p in periods]),
    ... ])
    >>> JournalPeriod.click(journal_periods, 'reopen')
    >>> statement.reload()
    >>> statement_line, = statement.lines
    >>> statement_line.click('cancel')
    >>> len(statement_line.counterpart_lines)
    1
    >>> not statement_line.counterpart_lines[0].reconciliation
    True
    >>> len(statement_line.bank_lines)
    0
    >>> receivable.reload()
    >>> receivable.balance
    Decimal('80.00')

Create another moves::

    >>> move2 = Move()
    >>> move2.period = period
    >>> move2.journal = account_journal
    >>> move2.date = period.start_date
    >>> line = move2.lines.new()
    >>> line.account = receivable
    >>> line.debit = Decimal('100.0')
    >>> line.party = party
    >>> line2 = move2.lines.new()
    >>> line2.account = revenue
    >>> line2.credit = Decimal('100.0')
    >>> move2.click('post')
    >>> reconcile2, = [l for l in move2.lines if l.account == receivable]

    >>> move3 = Move()
    >>> move3.period = period
    >>> move3.journal = account_journal
    >>> move3.date = period.start_date
    >>> line = move3.lines.new()
    >>> line.account = receivable
    >>> line.debit = Decimal('200.0')
    >>> line.party = party
    >>> line2 = move3.lines.new()
    >>> line2.account = revenue
    >>> line2.credit = Decimal('200.0')
    >>> move3.click('post')
    >>> reconcile3, = [l for l in move3.lines if l.account == receivable]

Create bank statement with joined move::

    >>> statement_journal.joined_move = True
    >>> statement_journal.save()
    >>> statement2 = BankStatement(journal=statement_journal, date=now)
    >>> statement_line = statement2.lines.new()
    >>> statement_line.date = now
    >>> statement_line.description = 'Statement Line 2'
    >>> statement_line.amount = Decimal('300.0')
    >>> statement2.click('confirm')
    >>> statement_line, = statement2.lines
    >>> reconcile2 = MoveLine(reconcile2.id)
    >>> reconcile3 = MoveLine(reconcile3.id)
    >>> statement_line.counterpart_lines.extend([reconcile2, reconcile3])
    >>> reconcile2 = MoveLine(reconcile2.id)
    >>> reconcile3 = MoveLine(reconcile3.id)
    >>> statement2.save()
    >>> statement_line, = statement2.lines
    >>> statement_line.click('post')
    >>> bank_line, = statement_line.bank_lines
    >>> bank_move_line = bank_line.move_line
    >>> bank_move_line.move_description_used == statement_line.description
    True
    >>> (bank_move_line.debit, bank_move_line.credit)
    (Decimal('300.0'), Decimal('0.0'))
    >>> bank_move_line.account == statement_journal.account
    True
    >>> move = bank_move_line.move
    >>> move.date == statement_line.date.date()
    True
    >>> move.origin == statement2
    True
    >>> move.journal == statement_journal.journal
    True
    >>> len(move.lines)
    3
    >>> line1, = [l for l in move.lines if l.account == receivable and l.credit == Decimal('100.0')]
    >>> line2, = [l for l in move.lines if l.account == receivable and l.credit == Decimal('200.0')]
    >>> line3, = [l for l in move.lines if l.account == cash]
    >>> rline1, = [l for l in line1.reconciliation.lines if l != line1]
    >>> rline1 == reconcile2
    True
    >>> rline2, = [l for l in line2.reconciliation.lines if l != line2]
    >>> rline2 == reconcile3
    True
    >>> (line3.debit, line3.credit)
    (Decimal('300.0'), Decimal('0.0'))
