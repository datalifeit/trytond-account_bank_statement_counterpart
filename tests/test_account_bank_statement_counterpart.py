# This file is part of the account_bank_statement_counterpart module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountBankStatementCounterpartTestCase(ModuleTestCase):
    'Test Account Bank Statement Counterpart module'
    module = 'account_bank_statement_counterpart'


del ModuleTestCase
