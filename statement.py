# This file is part account_bank_statement_counterpart the COPYRIGHT file at
# the top level of this repository contains the full copyright notices and
# license terms.
from decimal import Decimal

from sql.aggregate import Sum
from sql.conditionals import Coalesce
from trytond import backend
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import Index, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Equal, Eval, Not
from trytond.tools import reduce_ids
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView

CONFIRMED_STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft'))
    }
POSTED_STATES = {
    'readonly': Not(Equal(Eval('state'), 'confirmed'))
    }

_ZERO = Decimal('0.0')


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.bank.statement.line'

    counterpart_lines = fields.One2Many('account.move.line',
        'bank_statement_line_counterpart', 'Counterpart',
        states=POSTED_STATES, domain=[
            ('move.company', '=', Eval('company')),
            ('account.reconcile', '=', True),
            ('move_state', '=', 'posted'),
            ],
        add_remove=[
            ('reconciliation', '=', None),
            ('bank_statement_line_counterpart', '=', None),
            ('move_state', '=', 'posted'),
            ('account.reconcile', '=', True),
            ])
    account_date = fields.Function(fields.DateTime('Account Date'),
        'get_date_utc', searcher='search_date_utc',
        setter='set_date_utc')
    account_date_utc = fields.DateTime('Account Date UTC',
        states={
            'required': Bool(Eval('counterpart_lines')),
        })

    @classmethod
    def __register__(cls, module_name):
        table = backend.TableHandler(cls, module_name)

        # Migration: rename account_date into account_date_utc
        if (table.column_exist('account_date')
                and not table.column_exist('account_date_utc')):
            table.column_rename('account_date', 'account_date_utc')

        super(StatementLine, cls).__register__(module_name)

    @fields.depends('date')
    def on_change_with_account_date(self):
        return self.date

    @classmethod
    def create(cls, vlist):
        for vals in vlist:
            if vals.get('account_date') or not vals.get('date', None):
                continue
            vals['account_date'] = vals['date']
        return super(StatementLine, cls).create(vlist)

    def _search_counterpart_line_reconciliation_domain(self):
        return [
            ('company', '=', self.statement.company),
            ('reconciliation', '=', None),
            ('bank_statement_line_counterpart', '=', None),
            ('move_state', '=', 'posted'),
            ('account.reconcile', '=', True),
            ]

    def _search_counterpart_line_reconciliation(self):
        search_amount = self.company_amount - self.moves_amount
        if search_amount == _ZERO:
            return

        MoveLine = Pool().get('account.move.line')
        domain = self._search_counterpart_line_reconciliation_domain()
        if search_amount > 0:
            domain.append(('debit', '=', abs(search_amount)))
        else:
            domain.append(('credit', '=', abs(search_amount)))
        lines = MoveLine.search(domain)

        if len(lines) == 1:
            line, = lines
            line.bank_statement_line_counterpart = self
            line.save()

    def _search_reconciliation(self):
        super(StatementLine, self)._search_reconciliation()
        self._search_counterpart_line_reconciliation()

    def _check_period(lines):
        Period = Pool().get('account.period')

        company_ids = set(l.company.id for l in lines)
        check_lines = dict((c, set()) for c in company_ids)

        for line in lines:
            check_lines.setdefault(
                line.company.id, set()).add(line.account_date)

        for company, dates in check_lines.items():
            for date in set(dates):
                Period.find(company, date=date.date())

    @classmethod
    @ModelView.button
    def post(cls, statement_lines):
        cls._check_period(statement_lines)
        for st_line in statement_lines:
            if not st_line.journal.joined_move:
                for line in st_line.counterpart_lines:
                    st_line.create_move(line)
        super(StatementLine, cls).post(statement_lines)

    @classmethod
    @ModelView.button
    def cancel(cls, statement_lines):
        cls._check_period(statement_lines)
        cls.reset_counterpart_move(statement_lines)
        to_write = []
        for st_line in statement_lines:
            to_write.extend(([st_line], {'state': 'cancelled'}))
        if to_write:
            cls.write(*to_write)
        super(StatementLine, cls).cancel(statement_lines)

    @fields.depends('state', 'counterpart_lines')
    def on_change_with_moves_amount(self, name=None):
        amount = super(StatementLine, self).on_change_with_moves_amount(name)
        if self.state == 'posted':
            return amount

        amount += sum((l.debit or _ZERO) - (l.credit or _ZERO) for l in
            self.counterpart_lines)
        if self.company_currency:
            amount = self.company_currency.round(amount)
        return amount

    @classmethod
    def _get_moves_amount(cls, lines):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        move_line = MoveLine.__table__()
        cursor = Transaction().connection.cursor()

        res = super()._get_moves_amount(lines)
        sublines = [l for l in lines if l.state != 'posted']

        cursor.execute(*move_line.select(
                move_line.bank_statement_line_counterpart,
                Sum(Coalesce(move_line.debit, 0)
                    - Coalesce(move_line.credit, 0)),
                where=(reduce_ids(move_line.bank_statement_line_counterpart,
                    list(map(int, sublines)))),
                group_by=move_line.bank_statement_line_counterpart)
        )
        for line_id, amount in cursor.fetchall():
            res[line_id] += amount
        return res

    @classmethod
    def reset_counterpart_move(cls, lines):
        pool = Pool()
        Reconciliation = pool.get('account.move.reconciliation')
        BankReconciliation = pool.get('account.bank.reconciliation')
        Move = pool.get('account.move')

        delete_moves = []
        delete_reconciliation = []
        for line in lines:
            for bank_line in line.bank_lines:
                if bank_line.move_origin in (line, line.statement):
                    move = bank_line.move_line.move
                    reconciliations = [ml.reconciliation
                        for ml in move.lines if ml.reconciliation]
                    delete_reconciliation.extend(reconciliations)
                    delete_moves.append(move)
        delete_bank_reconciliation = []
        for move in delete_moves:
            for line in move.lines:
                delete_bank_reconciliation.extend(line.bank_lines)
        with Transaction().set_context(from_account_bank_statement_line=True):
            if delete_bank_reconciliation:
                BankReconciliation.delete(delete_bank_reconciliation)
            if delete_reconciliation:
                delete_reconciliation = list(set(delete_reconciliation))
                Reconciliation.delete(delete_reconciliation)
            if delete_moves:
                Move.delete(delete_moves)

    def create_move(self, line):
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')
        Period = pool.get('account.period')

        if line.reconciliation:
            return

        # Create Move
        period_id = Period.find(self.company, date=self.account_date.date())
        move_lines = self._get_counterpart_move_lines(line)
        move = Move(
            company=self.statement.company,
            origin=self.statement,
            period=period_id,
            journal=self.journal.journal,
            lines=move_lines,
            date=self.account_date.date())
        move.save()
        Move.post([move])

        journal = self.journal
        account = journal.account

        # Reconcile lines
        counterparts = [x for x in move.lines if x.account != account]
        if not counterparts:
            raise UserError(gettext(
                'account_bank_statement_counterpart.not_found_counterparts'))
        Line.reconcile([counterparts[0], line])

        # Assign line to Transactions
        st_move_line, = [x for x in move.lines if x.account == account]
        bank_line, = st_move_line.bank_lines
        bank_line.bank_statement_line = self
        bank_line.save()
        self.save()

    def _get_counterpart_move_lines(self, line):
        pool = Pool()
        MoveLine = pool.get('account.move.line')

        move_lines = []
        # Generate counterpart line
        counterpart = self.get_counterpart_line(line)

        # Generate Bank Line.
        journal = self.journal
        account = journal.account
        if not account:
            raise UserError(gettext(
                'account_bank_statement.account_statement_journal',
                journal=journal.rec_name))
        if not account.bank_reconcile:
            raise UserError(gettext(
                'account_bank_statement.account_not_bank_reconcile',
                journal=journal.rec_name))
        if line.account == account:
            raise UserError(gettext(
                'account_bank_statement.same_account',
                account=line.account.rec_name,
                line=line.rec_name,
                journal=journal.rec_name))

        amount = line.debit - line.credit
        bank_move = MoveLine(
            journal=journal.journal,
            description=self.description,
            debit=amount >= _ZERO and amount or _ZERO,
            credit=amount < _ZERO and -amount or _ZERO,
            account=account,
            origin=self,
            move_origin=self.statement,
            second_currency=counterpart.second_currency,
            amount_second_currency=counterpart.amount_second_currency
        )
        if account.party_required:
            bank_move.party = line.party or self.company.party

        move_lines.append(bank_move)
        move_lines.append(counterpart)
        return move_lines

    def get_counterpart_line(self, line):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Currency = Pool().get('currency.currency')

        counterpart = MoveLine(
                description=line.description,
                debit=line.credit,
                credit=line.debit,
                account=line.account,
                second_currency=None,
                amount_second_currency=None,
            )
        if line.account.party_required:
            counterpart.party = line.party
        if self.statement_currency != self.company_currency:
            with Transaction().set_context(date=self.date.date()):
                amount_second_currency = abs(Currency.compute(
                    self.company_currency, line.debit - line.credit,
                    self.statement_currency))
            counterpart.amount_second_currency = (amount_second_currency
                * (-1 if line.debit - line.credit > 0 else 1))
            counterpart.second_currency = self.statement_currency
        return counterpart

    def get_joined_move_lines(self):
        lines, new_lines = super().get_joined_move_lines()
        for line in self.counterpart_lines:
            if line.reconciliation:
                continue
            lines.append(line)
            new_lines.append(self.get_counterpart_line(line))
        return lines, new_lines

    def reconcile_joined_move_lines(self, line, new_line):
        pool = Pool()
        MoveLine = pool.get('account.move.line')

        super().reconcile_joined_move_lines(line, new_line)
        if line.__name__ == 'account.move.line':
            MoveLine.reconcile([line, new_line])

    def get_joined_bank_move_line(self, lines):
        journal = self.journal
        account = journal.account
        for line in self.counterpart_lines:
            if line.account == account:
                raise UserError(gettext('account_bank_statement.same_account',
                    account=line.account.rec_name,
                    line=line.rec_name,
                    journal=journal.rec_name))
        return super().get_joined_bank_move_line(lines)


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def check_modify(cls, *args, **kwargs):
        if Transaction().context.get('from_account_bank_statement_line',
                False):
            return
        return super(Move, cls).check_modify(*args, **kwargs)


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    bank_statement_line_counterpart = fields.Many2One(
        'account.bank.statement.line', 'Bank Statement Line Counterpart',
        readonly=True)

    @classmethod
    def __setup__(cls):
        super(MoveLine, cls).__setup__()
        table = cls.__table__()
        cls._check_modify_exclude.add('bank_statement_line_counterpart')

        cls._sql_indexes.add(
            Index(table,
                (table.bank_statement_line_counterpart, Index.Equality())))

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        default['bank_statement_line_counterpart'] = None
        return super(MoveLine, cls).copy(lines, default=default)

    @classmethod
    def check_modify(cls, *args, **kwargs):
        if Transaction().context.get('from_account_bank_statement_line',
                False):
            return
        return super(MoveLine, cls).check_modify(*args, **kwargs)


class UnreconcileLinesWarning(ModelView):
    """Unreconcile Lines Warning"""
    __name__ = 'account.move.unreconcile_lines.warning'

    reconciliations = fields.One2Many(
        'account.move.reconciliation.bank_statement_lines', None,
        'Reconciliations', readonly=True)


class ReconciliationBank(ModelView):
    """Reconciliation - Bank Statement Line"""
    __name__ = 'account.move.reconciliation.bank_statement_lines'

    reconciliation = fields.Many2One('account.move.reconciliation',
        'Reconciliation', readonly=True)
    bank_statement_lines = fields.One2Many('account.bank.statement.line',
        None, 'Bank Statement Lines', readonly=True)


class UnreconcileLines(metaclass=PoolMeta):
    __name__ = 'account.move.unreconcile_lines'

    warning = StateView('account.move.unreconcile_lines.warning',
        'account_bank_statement_counterpart.'
        'unreconcile_lines_warning_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'continue_', 'tryton-ok', default=True)])
    continue_ = StateTransition()
    to_warning = {}

    def transition_unreconcile(self):
        self.to_warning = {}
        reconciliations = self._get_reconciliations()
        for reconciliation in reconciliations:
            for line in reconciliation.lines:
                for move_line in line.move.lines:
                    for bank_line in move_line.bank_lines:
                        if bank_line.bank_statement_line:
                            self.to_warning.setdefault(reconciliation, [])
                            self.to_warning[reconciliation].append(
                                bank_line.bank_statement_line)
        if self.to_warning:
            return 'warning'
        return super().transition_unreconcile()

    def default_warning(self, fields):
        return {'reconciliations': [{
            'reconciliation': reconciliation.id,
            'bank_statement_lines': [line.id for line in lines]
        } for reconciliation, lines in self.to_warning.items()]}

    def transition_continue_(self):
        pool = Pool()
        Reconciliation = pool.get('account.move.reconciliation')

        reconciliations = self._get_reconciliations()
        Reconciliation.delete(reconciliations)
        return 'end'

    def _get_reconciliations(self):
        pool = Pool()
        Line = pool.get('account.move.line')

        lines = Line.browse(Transaction().context['active_ids'])
        return list(set(x.reconciliation for x in lines if x.reconciliation))
